<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'defaultIconHtml' => false,
                'items' => [
                    [
                        'label' => 'Меню',
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label' => 'Home',
                        'icon' => 'home',
                        'url' => ['site/index'],
                    ],
                    [
                        'label' => 'Frontend Home',
                        'icon' => 'home',
                        'url' => \yii::$app->urlManagerFrontend->createUrl(['site/index']),
                        'template' => '<a href="{url}" target="_blank">{icon} {label}</a>',
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
